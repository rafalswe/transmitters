package zstd.zad1;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TransmitterGenerator {
    private double citySpread;
    private double transmitterSpread;
    private Random generator;

    public TransmitterGenerator(double citySpread, double transmitterSpread) {
        this.citySpread = citySpread;
        this.transmitterSpread = transmitterSpread;
        generator = new Random();
    }

    public List<Transmitter> generateTransmitters(int n) {
        int counter = n;
        List<Transmitter> resultList = new LinkedList<>();
        while (counter > 0) {
            Transmitter current = generateTransmitter();
            if (isTransmitterInCity(current)) {
                counter--;
                current.setId(counter);
                resultList.add(current);
            }
        }
        Collections.reverse(resultList);
        return resultList;
    }

    private boolean isTransmitterInCity(Transmitter current) {
        return PointsUtils.distance(new Point(0, 0), current.getPoint()) <= citySpread;
    }

    private Transmitter generateTransmitter() {
        return new Transmitter(drawPoint(), transmitterSpread);
    }

    private Point drawPoint() {
        return new Point(drawCoordinate(), drawCoordinate());
    }

    private double drawCoordinate() {
        return generator.nextDouble() * citySpread * 2 - citySpread;
    }

    public double getCitySpread() {
        return citySpread;
    }

    public void setCitySpread(double citySpread) {
        this.citySpread = citySpread;
    }

    public double getTransmitterSpread() {
        return transmitterSpread;
    }

    public void setTransmitterSpread(double transmitterSpread) {
        this.transmitterSpread = transmitterSpread;
    }
}
