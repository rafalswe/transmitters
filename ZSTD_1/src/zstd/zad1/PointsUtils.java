package zstd.zad1;

public class PointsUtils {
    private PointsUtils() {
    }
    
    public static double distance(Point A, Point B) {
        return Math.sqrt(Math.pow(A.getX() - B.getX(), 2) + Math.pow(A.getY() - B.getY(), 2));
    }
}
