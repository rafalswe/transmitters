package zstd.zad1;

import java.util.List;

import net.sourceforge.gxl.GXLEdge;
import net.sourceforge.gxl.GXLGraph;
import net.sourceforge.gxl.GXLNode;

public class GraphGenerator {
    private List<Transmitter> transmitters;

    public GraphGenerator(List<Transmitter> transmitters) {
        this.transmitters = transmitters;
    }

    public GXLGraph generateGraph() {
        GXLGraph graph = new GXLGraph("transmitters");
        addNodes(graph);
        addEdges(graph);
        return graph;
    }

    private void addNodes(GXLGraph graph) {
        for (Transmitter transmitter : transmitters) {
            graph.add(new GXLNode(transmitter.getId() + ""));
        }
    }

    private void addEdges(GXLGraph graph) {
        for (int i = 0; i < transmitters.size(); i++) {
            Transmitter currentI = transmitters.get(i);
            for (int j = 0; j < transmitters.size(); j++) {
                if (i == j) {
                    continue;
                }
                Transmitter currentJ = transmitters.get(j);
                if (areDependence(currentI, currentJ)) {
                    graph.add(new GXLEdge(currentI.getId() + "", currentJ.getId() + ""));
                }
            }
        }
    }

    private boolean areDependence(Transmitter a, Transmitter b) {
        return PointsUtils.distance(a.getPoint(), b.getPoint()) < a.getSpread() + b.getSpread();
    }
}
