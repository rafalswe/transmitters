package zstd.zad1;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import net.sourceforge.gxl.GXLDocument;
import net.sourceforge.gxl.GXLGraph;

public class MainZad1 {
        
    
    public static void main(String[] args) throws IOException {
        System.out.println("Rafal Swedra; 384056; Projekt na przedmiot Struktury Dyskretne.");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczb� anten");
        int numberOfTransmitters = Integer.valueOf(scanner.nextLine());
        System.out.println("Podaj promie� zasi�gu anteny");
        double transmitterSpread = Double.valueOf(scanner.nextLine());
        System.out.println("Podaj promie� miasta");
        double citySpread = Double.valueOf(scanner.nextLine());
        
        TransmitterGenerator generator = new  TransmitterGenerator(citySpread, transmitterSpread);
        List<Transmitter> generatedTransmitters = generator.generateTransmitters(numberOfTransmitters);
        
        GraphGenerator graphGenerator = new GraphGenerator(generatedTransmitters);
        
        GXLGraph generatedGraph = graphGenerator.generateGraph();
        writeGraphToFile(generatedGraph);
        System.out.println("DONE");
    }

    private static void writeGraphToFile(GXLGraph generatedGraph) throws IOException {
        GXLDocument gxlDocument = new GXLDocument();
        gxlDocument.getDocumentElement().add(generatedGraph);
        gxlDocument.write(new File("graph.gxl"));
    }

    private static void print(List<Transmitter> generatedTransmitters) {
        for (Transmitter transmitter : generatedTransmitters) {
            System.out.println(transmitter.toString());
        }
    }
    
}
