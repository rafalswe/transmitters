package zstd.zad1;

public class Transmitter {
    private Point point;
    private double spread;
    private int id;

    public Transmitter(Point point, double spread) {
        this.point = point;
        this.spread = spread;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public double getSpread() {
        return spread;
    }

    public void setSpread(double spread) {
        this.spread = spread;
    }
    
    public double getX() {
        return point.getX();
    }
    
    public double getY() {
        return point.getY();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Transmitter [point=" + point + ", spread=" + spread + ", id=" + id + "]";
    }
}
