package zstd.zad2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.SAXException;

import net.sourceforge.gxl.GXLDocument;
import net.sourceforge.gxl.GXLEdge;
import net.sourceforge.gxl.GXLElement;
import net.sourceforge.gxl.GXLGXL;
import net.sourceforge.gxl.GXLGraph;
import net.sourceforge.gxl.GXLNode;

public class MainZad2 {
    private static List<GXLEdge> edges = new ArrayList<>();
    private static List<GXLNode> nodes = new ArrayList<>();

    public static void main(String[] args) throws IOException, SAXException {
        System.out.println("Rafal Swedra; 384056; Projekt na przedmiot Struktury Dyskretne.");
        readGXL();   
        Graph graph = new Graph();
        graph.addNodes(nodes);
        graph.addEdges(edges);
        graph.addLabelsToNodes();
        System.out.println("Liczba potrzebnych czestotliwosci: " + graph.getNumberOfFrequencies());
    }

    private static void readGXL() throws IOException, SAXException {
        GXLDocument gxlDocument = new GXLDocument(new File("graph.gxl"));
        GXLGXL gxlGxl = gxlDocument.getDocumentElement();
        GXLGraph graph = gxlGxl.getGraphAt(0);
        for (int i = 0; i < graph.getChildCount(); i++) {
            GXLElement elem = graph.getGraphElementAt(i);
            if( elem instanceof GXLNode ) {
                nodes.add((GXLNode) elem);
            } else if( elem instanceof GXLEdge ) {
                edges.add((GXLEdge) elem);
            }
        }
    }
}
