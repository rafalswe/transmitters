package zstd.zad2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import net.sourceforge.gxl.GXLEdge;
import net.sourceforge.gxl.GXLNode;

public class Graph {
    private Map<Integer, Node> nodes = new HashMap<>();
    private List<Integer> labels = new ArrayList<Integer>();

    public Graph() {
        labels.add(0);
    }

    public void addNodes(List<GXLNode> gxlNodes) {
        for (GXLNode gxlNode : gxlNodes) {
            String currentId = gxlNode.getID();
            nodes.put(Integer.valueOf(currentId), new Node(currentId));
        }
    }

    public void addEdges(List<GXLEdge> gxlEdges) {
        for (GXLEdge gxlEdge : gxlEdges) {
            Node sourceNode = nodes.get(Integer.valueOf(gxlEdge.getSourceID()));
            Node targetNode = nodes.get(Integer.valueOf(gxlEdge.getTargetID()));
            sourceNode.addConnectedNode(targetNode);
        }
    }

    public void addLabelsToNodes() {
        Queue<Node> queue = new LinkedList<>();
        Integer firstWithoutLabel = getFirstNodeWithOutLabel();
        while (firstWithoutLabel >= 0) {
            queue.add(nodes.get(firstWithoutLabel));
            while(!queue.isEmpty()) {
                Node currentNode = queue.poll();
                addLabelToNode(currentNode);
                addUnvisitedNodesToQueue(currentNode, queue);
            }
            firstWithoutLabel = getFirstNodeWithOutLabel();
        }
    }

    private Integer getFirstNodeWithOutLabel() {
        for (Integer id : nodes.keySet()) {
            if (nodes.get(id)
                     .getLabelFrequency() == null) {
                return id;
            }
        }
        return -1;
    }

    private void addLabelToNode(Node currentNode) {
        for (Integer label : labels) {
            if(currentNode.isLabelFree(label)) {
                currentNode.setLabelFrequency(label);
                return;
            }
        }
        currentNode.setLabelFrequency(getNewLabel());
    }
    private Integer getNewLabel() {
        Integer newLabel = labels.size();
        labels.add(newLabel);
        return newLabel;
    }
    
    private void addUnvisitedNodesToQueue(Node currentNode, Queue<Node> queue) {
        queue.addAll(currentNode.getUnvisitedNodes());
    }
    
    public int getNumberOfFrequencies(){
        return labels.size();
    }

}
