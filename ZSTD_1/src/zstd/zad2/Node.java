package zstd.zad2;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private String id;
    private List<Node> connectedNodes = new ArrayList<>();
    private Integer labelFrequency = null;

    public Node(String id) {
        this.id = id;
    }
    
    public List<Node> getConnectedNodes() {
        return connectedNodes;
    }

    public String getId() {
        return id;
    }

    public Integer getLabelFrequency() {
        return labelFrequency;
    }

    public void setLabelFrequency(Integer labelFrequency) {
        this.labelFrequency = labelFrequency;
    }
    
    public void addConnectedNode(Node node) {
        connectedNodes.add(node);
    }
    
    public boolean isLabelFree(Integer label) {
        for (Node node : connectedNodes) {
           if(label.equals(node.getLabelFrequency())) {
               return false;
           }
        }
        return true;
    }

    public List<Node> getUnvisitedNodes() {
        List<Node> unvisitedNodes = new ArrayList<>();
        for (Node node : connectedNodes) {
            if(node.getLabelFrequency() == null) {
                unvisitedNodes.add(node);
            }
        }
        return unvisitedNodes;
    }

}
